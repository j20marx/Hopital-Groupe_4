﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


//Script a associer au patient1
public class IsOnContactMamie : MonoBehaviour
{
    public float chronoPiqure = 0.0f;
    public float chronoPerfusion = 0.0f;
    public float chronoNourriture = 0.0f;
    public float chronoReanimation = 0.0f;

    public Text textPiqure;
    public Text textPerfusion;
    public Text textNourriture;
    public Text textReanimation;

    public HealthBar_1 healthBar;



    public void OnTriggerStay(Collider other)
    {
        if (other.gameObject.name == "Syringe_Mamie")
        {
            chronoPiqure += Time.deltaTime;
            
            if (chronoPiqure >= 5.0f & chronoPiqure <= 6.0f)
            {
                textPiqure.enabled = false;
                healthBar.Heal(100);
            }
        }
        if (other.gameObject.name == "Perfusion_Mamie") 
        {
            chronoPerfusion += Time.deltaTime;
            if (chronoPerfusion >= 5.0f)
            {
                textPerfusion.enabled = false;
                healthBar.Heal(100);
            }
        }
        if (other.gameObject.name == "Food_mamie")
        {
            chronoNourriture += Time.deltaTime;
            if (chronoNourriture >= 5.0f)
            {
                textNourriture.enabled = false;
                healthBar.Heal(100);
            }
        }
        if(other.gameObject.name == "Reanimer_Mamie")
        {
            chronoReanimation += Time.deltaTime;
            if (chronoReanimation >= 5.0f)
            {
                textReanimation.enabled = false;
                healthBar.Heal(100);
            }
        }

        
    }

    public void OnTriggerExit(Collider other)
    {
        chronoPiqure = 0.0f;
        chronoPerfusion = 0.0f;
        chronoNourriture = 0.0f;
    }

}
