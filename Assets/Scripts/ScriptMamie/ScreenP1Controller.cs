﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class ScreenP1Controller : MonoBehaviour
{
    //Valable pour un seul patient : la mamie

    public Text textPiqure;
    public Text textPerfusion;
    public Text textNourriture;
    public Text textReanimation;

    public float chrono;

    public GameObject mamie;

    public GameObject syringeMamie;
    public GameObject perfusionMamie;
    public GameObject reanimerMamie;
    public GameObject foodMamie;

    public Texture rouge;
    public Texture blanc;
    public Texture marron;


    public HealthBar_1 healthBar;

 

    public void UpdateTasksToDo()
    {
        if(10.0f < chrono & chrono < 15.0f)
        {
            textPiqure.enabled = true;
            healthBar.Heal(75);
            syringeMamie.GetComponent<Renderer>().material.mainTexture = rouge; 

        }

        if(textPiqure.enabled == false)
        {
            syringeMamie.GetComponent<Renderer>().material.mainTexture = blanc;

        }

        if (chrono > 35.0f & chrono < 36.0f & textPiqure.enabled == true)
        {
            textPiqure.enabled = false;
            textPerfusion.enabled = true;
            perfusionMamie.GetComponent<Renderer>().material.mainTexture = rouge;
            healthBar.Heal(50);
        }
        if (textPerfusion.enabled == false)
        {
            perfusionMamie.GetComponent<Renderer>().material.mainTexture = blanc;

        }

        if (chrono > 60.0f & chrono < 61.0f & textPerfusion.enabled == true)
        {

            textPerfusion.enabled = false;
            textReanimation.enabled = true;
            reanimerMamie.GetComponent<Renderer>().material.mainTexture = rouge;
            healthBar.Heal(25);

        }
        if (textReanimation.enabled == false)
        {
            reanimerMamie.GetComponent<Renderer>().material.mainTexture = marron;

        }

        if (chrono > 90.0f & chrono< 91.0f & textReanimation.enabled == true)
        {
            healthBar.Heal(0);
            SceneManager.LoadScene("SceneGameOver");
        }



        //Deuxième série de tâches
        if (100.0f < chrono & chrono < 101.0f)
        {
            textNourriture.enabled = true;
            healthBar.Heal(75);
        }
        

        if (150.0f < chrono & chrono < 151.0f)
        {
            textNourriture.enabled = false;
            textPiqure.enabled = true;
            syringeMamie.GetComponent<Renderer>().material.mainTexture = rouge;
            healthBar.Heal(50);
        }
        if (textPiqure.enabled == false)
        {
            syringeMamie.GetComponent<Renderer>().material.mainTexture = blanc;

        }

        if (chrono > 200.0f & chrono < 201.0f & textPiqure.enabled == true)
        {
            textPiqure.enabled = false;
            textPerfusion.enabled = true;
            perfusionMamie.GetComponent<Renderer>().material.mainTexture = rouge;
            healthBar.Heal(25);
        }
        if (textPerfusion.enabled == false)
        {
            perfusionMamie.GetComponent<Renderer>().material.mainTexture = blanc;

        }

        if (chrono > 220.0f & chrono < 221.0f & textPerfusion.enabled == true)
        {
            healthBar.Heal(0);

            SceneManager.LoadScene("SceneGameOver");
        }

    }




    // Start is called before the first frame update
    void Start()
    {
    
        textPiqure.enabled = false;
        textPerfusion.enabled = false;
        textNourriture.enabled = false;
        textReanimation.enabled = false;
     
    }

    // Update is called once per frame
    void Update()
    {
        chrono += Time.deltaTime;
        UpdateTasksToDo();
    }
}
