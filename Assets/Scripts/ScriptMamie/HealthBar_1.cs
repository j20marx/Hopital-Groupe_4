using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public class HealthBar_1 : MonoBehaviour {

private int hp;
private int hpmax;

[SerializeField]
private Image healthbar_1;

void Start()
{
	hp = 100;
	hpmax = 100;
}


// Inflige des dégâts
public void TakeDamage(int damages)
{
hp = hp - damages;
UpdateHealth();
}
// Soigne le joueur
public void Heal(int heal)
{
hp += heal;
UpdateHealth();
}
// Soigne le joueur entièrement
public void TotalHeal()
{
hp = 100;
UpdateHealth();
}

// Actualise les points de vie pour rester entre 0 et hpmax
private void UpdateHealth()
{
hp = Mathf.Clamp(hp, 0, hpmax);
float amount = (float)hp / hpmax;
healthbar_1.fillAmount= amount; 
}
}
