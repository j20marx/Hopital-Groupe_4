﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject gameOverText;
    public bool IsGameActive;
    private float chrono;
    private float f;

    // Start is called before the first frame update
    void Start()
    {
        chrono=0.0f;
        f=1.0f; //changer f selon temps de l'update
    }

    // Update is called once per frame
    void Update()
    {

        if (IsGameActive){
            chrono+=f;
        }
        

        if (IsGameActive==false){
            GameOver(chrono);
            //rendre actif le score 
            //rendre actif boutton restart 
        }


    }

    public void GameOver(float score)
    {
        gameOverText.SetActive(true);
    
    }

    public float getChrono(){
        return chrono;
    }

    
}
