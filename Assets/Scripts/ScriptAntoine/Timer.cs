﻿using UnityEngine;
using TMPro;

public class Timer
{
    private int timer_minutes;
    //private TextMeshPro text;
    private float current_time;
    private float start_time;
    private bool isStoped;
    private bool isReset;

    public Timer(int timer)
    {
        timer_minutes = timer;
        isStoped = true;
        isReset = true;
        current_time = timer_minutes * 60;
        //text.SetText(ConvertTime(current_time));
    }
    // Start is called before the first frame update
    void Start(){}

    // Update is called once per frame
    void Update(){}

    public void UpdateTimer()
    {
        if (!isStoped)
        {
            current_time = (timer_minutes * 60) - (Time.time - start_time);
            if (current_time > 0)
            {
                //text.SetText(ConvertTime(current_time));
            }
            else
            {
                //text.SetText("00:00");
                isStoped = true;
            }
        }
    }

    public string ConvertTime(float t)
    {
        string minutes = Mathf.Floor(t / 60).ToString("00");
        string seconds = Mathf.Floor(t % 60).ToString("00");
        return minutes + ":" + seconds;
    }

    public bool IsStoped()
    {
        return isStoped;
    }

    public bool IsReset()
    {
        return isReset;
    }

    public void StartTimer()
    {
        if (isStoped && isReset)
        {
            isStoped = false;
            isReset = false;
            start_time = Time.time;
        }
    }

    public void ResetTimer()
    {
        isStoped = true;
        isReset = true;
        current_time = timer_minutes * 60;
        //text.SetText(ConvertTime(current_time));
    }

    public void StopTimer()
    {
        isStoped = true;
    }

    public int getTimer()
    {
        return timer_minutes;
    }

    public float GetCurrentTime()
    {
        return current_time;
    }
}
