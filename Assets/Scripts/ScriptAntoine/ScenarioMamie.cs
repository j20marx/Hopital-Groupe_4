﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScenarioMamie : MonoBehaviour
{
    public HealthBar_1 health;
    
    Timer timer1 = new Timer(1);
    public TextMeshPro text1;
    public TextMeshPro text_timer1 ;
    private MamieCollider nouilles;
    private bool isDone1;
    private int i1 = 0;

    // Start is called before the first frame update
    void Start()
    {

        text1.SetText("Donner repas");
        text1.enabled = false;
        text_timer1.enabled = false;
        isDone1 = false;

    }

    // Update is called once per frame
    void Update()
    {
        if (Chrono.chrono > 5 && Chrono.chrono<65)
        {
            if (MamieCollider.isTriggeredNoodles == false)
            {
                text1.enabled = true;
                text1.color = Color.red;
                timer1.StartTimer();
                timer1.UpdateTimer();
                text_timer1.SetText(timer1.ConvertTime(timer1.GetCurrentTime()));
                text_timer1.enabled = true;
                text_timer1.color = Color.red;
            }
            else
            {
                text1.color = Color.green;
                text_timer1.enabled = false;
                isDone1 = true;

            }
        }
        if (Chrono.chrono > 65 && isDone1 == false && i1 == 0)
        {
            health.TakeDamage(25);
            i1 = 1;
        }
        
 

    }  
}
