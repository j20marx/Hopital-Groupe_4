﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Chrono : MonoBehaviour
{

    public static float chrono=0;
    private bool isStoped;
    private bool isReset;

    // Start is called before the first frame update
    void Start()
    {
        isStoped = true;
        isReset = true;
        GetComponent<Text>().text = ConvertTime(chrono);
    }

    // Update is called once per frame
    void Update()
    {
       
        if (!isStoped)
        {
            chrono += Time.deltaTime;
            GetComponent<Text>().text = ConvertTime(chrono);
        }

        
    }

    public string ConvertTime(float t)
    {
        string minutes = Mathf.Floor(t / 60).ToString("00");
        string seconds = Mathf.Floor(t % 60).ToString("00");
        return minutes + ":" + seconds;
    }

    
    public void StartTimer()
    {
        if (isStoped)
        {
            isStoped = false;
            isReset = false;
        }
    }


    public void ResetTimer()
    {
        isStoped = true;
        isReset = true;
        chrono = 0;
        GetComponent<Text>().text = ConvertTime(chrono);
    }

    public void Pause()
    {
        isStoped = true;
        isReset = false;
    }

}

