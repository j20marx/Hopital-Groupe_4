﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MamieCollider : MonoBehaviour
{

    public static bool isTriggeredNoodles = false;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "Mamie")
        {
            isTriggeredNoodles = true;
            Debug.Log("interaction!");
        }
    }

    public void ResetTrigger()
    {
        isTriggeredNoodles = false;
    }

    public bool GetTrigger()
    {
        return isTriggeredNoodles;
    }
}
