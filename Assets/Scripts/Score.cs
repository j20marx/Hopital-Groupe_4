﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    private Text score;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Text>().text = "Score:" + ConvertTime(Chrono.chrono);
        //score = GetComponent<Text>();
        //score.text("Score:" + ConvertTime(Chrono.chrono));
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Text>().text = "Score:" + ConvertTime(Chrono.chrono);
        //score = GetComponent<Text>();
        //score.SetText("Score:" + ConvertTime(Chrono.chrono));
    }

    public string ConvertTime(float t)
    {
        string minutes = Mathf.Floor(t / 60).ToString("00");
        string seconds = Mathf.Floor(t % 60).ToString("00");
        return minutes + ":" + seconds;
    }
}
