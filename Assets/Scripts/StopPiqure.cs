﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class StopPiqure : MonoBehaviour
{
    public InputAction ia;
    public Text textPiqure;
    
    public void stopTask()
    {
        textPiqure.enabled = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        ia.Enable();
        ia.performed += ctx => stopTask();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
