﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScreenP3Controller : MonoBehaviour
{
    //Valable pour un seul patient : la femme

    public Text textNourriture;
    public Text textTemperature;
    public Text textMedicament;
    public Text textPansement;
    public Text textToilette;
    public Text textTension;

    public GameObject foodFemme;
    public GameObject thermometreFemme;
    public GameObject medicamentFemme;
    public GameObject pansementFemme;
    public GameObject servietteFemme;
    public GameObject tensionFemme;

    public float chrono = 0.0f;

    public GameObject femme;

    public Healthbar_3 healthBar;

    public Texture rouge;
    public Texture blanc;
    public Texture marron;
    public Texture cyan;

    public void UpdateTasksToDo()
    {
        //Première série de tâches
        if(chrono > 60.0f & chrono < 61.0f)
        {
            healthBar.Heal(60);
            servietteFemme.GetComponent<Renderer>().material.mainTexture = rouge;
            textToilette.enabled = true;
        }
        if(textToilette.enabled == false)
        {
            servietteFemme.GetComponent<Renderer>().material.mainTexture = blanc;
        }


        if(chrono > 75.0f & chrono < 76.0f)
        {
            healthBar.Heal(30);
            textNourriture.enabled = true;
        }
        if(chrono > 100.0f & chrono < 101.0f & textNourriture.enabled == true)
        {
            SceneManager.LoadScene("SceneGameOver");
        }






        //Deuxieme série de tâches
        if(chrono > 130.0f & chrono < 131.0f)
        {
            healthBar.Heal(60);
            tensionFemme.GetComponent<Renderer>().material.mainTexture = rouge;
            textTension.enabled = true; 
        }
        if(textTension.enabled == false)
        {
            tensionFemme.GetComponent<Renderer>().material.mainTexture = cyan;
        }


        if (chrono > 150.0f & chrono < 151.0f & textTension.enabled == true)
        {
            healthBar.Heal(30);
            textTension.enabled = false;
            textTemperature.enabled = true;
            thermometreFemme.GetComponent<Renderer>().material.mainTexture = rouge;
        }
        if(textTemperature.enabled == false)
        {
            thermometreFemme.GetComponent<Renderer>().material.mainTexture = blanc;
        }







        //Troisième série de tâches
        if (chrono > 200.0f & chrono < 201.0f)
        {
            healthBar.Heal(75);
            pansementFemme.GetComponent<Renderer>().material.mainTexture = rouge;
            textPansement.enabled = true;
        }
        if(textPansement.enabled == false)
        {
            pansementFemme.GetComponent<Renderer>().material.mainTexture = blanc;
        }



        if(chrono > 220.0f & chrono < 221.0f & textPansement.enabled == true)
        {
            healthBar.Heal(50);
            textPansement.enabled = false;
            textMedicament.enabled = true;
            medicamentFemme.GetComponent<Renderer>().material.mainTexture = rouge;
        }
        if(textMedicament.enabled == false)
        {
            medicamentFemme.GetComponent<Renderer>().material.mainTexture = blanc;
        }


        if (chrono > 250.0f & chrono < 251.0f & textMedicament.enabled == true)
        {
            healthBar.Heal(25);
            textMedicament.enabled = false;
            textTemperature.enabled = true;
            thermometreFemme.GetComponent<Renderer>().material.mainTexture = rouge;
        }
        if (textTemperature.enabled == false)
        {
            thermometreFemme.GetComponent<Renderer>().material.mainTexture = blanc;
        }


        if (chrono > 280.0f & chrono < 281.0f &  textTemperature.enabled == true)
        {
            healthBar.Heal(0);
            thermometreFemme.GetComponent<Renderer>().material.mainTexture = blanc;
            SceneManager.LoadScene("SceneGameOver");
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        textTemperature.enabled = false;
        textMedicament.enabled = false;
        textPansement.enabled = false;
        textToilette.enabled = false;
        textTension.enabled = false;
        textNourriture.enabled = false;

    }

    // Update is called once per frame
    void Update()
    {
        chrono += Time.deltaTime;
        UpdateTasksToDo();
    }
}
