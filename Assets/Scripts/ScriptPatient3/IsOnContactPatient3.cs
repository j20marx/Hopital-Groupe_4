﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IsOnContactPatient3 : MonoBehaviour
{
    public float chronoToilette = 0.0f;
    public float chronoNourriture = 0.0f;
    public float chronoTemperature = 0.0f;
    public float chronoMedicament = 0.0f;
    public float chronoPansement = 0.0f;
    public float chronoTension = 0.0f;

    public Text textTemperature;
    public Text textTension;
    public Text textNourriture;
    public Text textMedicament;
    public Text textToilette;
    public Text textPansement;

    public Healthbar_3 healthBar;

    public void OnTriggerStay(Collider other)
    {
        if (other.gameObject.name == "Thermometre_Femme")
        {
            chronoTemperature += Time.deltaTime;
            gameObject.GetComponent<MeshRenderer>().material.color = Color.yellow;
            if (chronoTemperature >= 5.0f)
            {
                textTemperature.enabled = false;
                healthBar.Heal(100);
                gameObject.GetComponent<MeshRenderer>().material.color = Color.green;
            }
        }
        if (other.gameObject.name == "Tension_Femme")
        {
            chronoTension += Time.deltaTime;
            gameObject.GetComponent<MeshRenderer>().material.color = Color.yellow;
            if (chronoTension >= 5.0f)
            {
                textTension.enabled = false;
                healthBar.Heal(100);
                gameObject.GetComponent<MeshRenderer>().material.color = Color.green;
            }
        }
        if (other.gameObject.name == "Food_Femme")
        {
            chronoNourriture += Time.deltaTime;
            gameObject.GetComponent<MeshRenderer>().material.color = Color.yellow;
            if (chronoNourriture >= 5.0f)
            {
                textNourriture.enabled = false;
                healthBar.Heal(100);
                gameObject.GetComponent<MeshRenderer>().material.color = Color.green;
            }
        }
        if (other.gameObject.name == "Medicament_Femme") 
        {
            chronoMedicament += Time.deltaTime;
            gameObject.GetComponent<MeshRenderer>().material.color = Color.yellow;
            if (chronoMedicament >= 5.0f)
            {
                textMedicament.enabled = false;
                healthBar.Heal(100);
                gameObject.GetComponent<MeshRenderer>().material.color = Color.green;
            }
        }
        if (other.gameObject.name == "Towel_Femme")
        {
            chronoToilette += Time.deltaTime;
            gameObject.GetComponent<MeshRenderer>().material.color = Color.yellow;
            if (chronoToilette >= 5.0f)
            {
                textToilette.enabled = false;
                healthBar.Heal(100);
                gameObject.GetComponent<MeshRenderer>().material.color = Color.green;
            }
        }
        if (other.gameObject.name == "Bandage_Femme")
        {
            chronoPansement += Time.deltaTime;
            gameObject.GetComponent<MeshRenderer>().material.color = Color.yellow;
            if (chronoPansement >= 5.0f)
            {
                textPansement.enabled = false;
                healthBar.Heal(100);
                gameObject.GetComponent<MeshRenderer>().material.color = Color.green;
            }
        }
    }

    public void OnTriggerExit(Collider other)
    {
        gameObject.GetComponent<MeshRenderer>().material.color = new Color(0, 0, 0, 0);

        chronoTemperature = 0.0f;
        chronoTension = 0.0f;
        chronoNourriture = 0.0f;
        chronoMedicament = 0.0f;
        chronoToilette = 0.0f;
        chronoPansement = 0.0f;
    }

    
}
