﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScreenP2Controller : MonoBehaviour
{

    //Valable pour un seul patient : Homme

  
    public Text textPerfusion;
    public Text textNourriture;
    public Text textTemperature;
    public Text textMedicament;
    public Text textReanimation;
    public Text textPansement;

    public GameObject perfusionHomme;
    public GameObject foodHomme;
    public GameObject thermometreHomme;
    public GameObject medicamentHomme;
    public GameObject reanimerHomme;
    public GameObject pansementHomme;

    public float chrono = 0.0f;

    public GameObject homme;

    public HealthBar_2 healthBar;

    public Texture rouge;
    public Texture blanc;
    public Texture marron;

    public void UpdateTasksToDo()
    {
        //Première série de tâches
        if (25.0f < chrono & chrono < 26.0f)
        {
            textTemperature.enabled = true;
            healthBar.Heal(80);
            thermometreHomme.GetComponent<Renderer>().material.mainTexture = rouge;
              
        }
        if(textTemperature.enabled == false)
        {
            thermometreHomme.GetComponent<Renderer>().material.mainTexture = marron;
        }


        if (chrono > 55.0f & chrono < 56.0f & textTemperature.enabled == true)
        {
            healthBar.Heal(60);
            textTemperature.enabled = false;
            textMedicament.enabled = true;
            medicamentHomme.GetComponent<Renderer>().material.mainTexture = rouge;
        }
        if(textMedicament.enabled == false)
        {
            medicamentHomme.GetComponent<Renderer>().material.mainTexture = blanc;
        }


        if (chrono > 85.0f & chrono < 86.0f & textMedicament.enabled == true)
        {
            healthBar.Heal(40);
            textMedicament.enabled = false;
            textPerfusion.enabled = true;
            perfusionHomme.GetComponent<Renderer>().material.mainTexture = rouge;
        }
        if(textPerfusion.enabled == false)
        {
            perfusionHomme.GetComponent<Renderer>().material.mainTexture = blanc;
        }


        if (chrono > 105.0f & chrono < 106.0f & textPerfusion.enabled == true)
        {
            healthBar.Heal(20);
            textPerfusion.enabled = false;
            textReanimation.enabled = true;
            reanimerHomme.GetComponent<Renderer>().material.mainTexture = rouge;
        }
        if(textReanimation.enabled == false)
        {
            reanimerHomme.GetComponent<Renderer>().material.mainTexture = marron;
        }


        if (chrono > 120.0f & chrono < 121.0f & textReanimation == true)
        {
            healthBar.Heal(0);
            reanimerHomme.GetComponent<Renderer>().material.mainTexture = marron;
            //SceneManager.LoadScene("SceneGameOver");
        }







        //Deuxième série de tâches
        if (chrono > 180.0f & chrono < 181.0f)
        {
            healthBar.Heal(80);
            textPansement.enabled = true;
            pansementHomme.GetComponent<Renderer>().material.mainTexture = rouge;
        }
        if(textPansement.enabled == false)
        {
            pansementHomme.GetComponent<Renderer>().material.mainTexture = blanc;
        }


        if (chrono > 200.0f & chrono < 201.0f & textPansement.enabled == true)
        {
            healthBar.Heal(60);
            textPansement.enabled = false;
            textMedicament.enabled = true;
            medicamentHomme.GetComponent<Renderer>().material.mainTexture = rouge;
        }
        if(textMedicament.enabled == false)
        {
            medicamentHomme.GetComponent<Renderer>().material.mainTexture = blanc;
        }


        if (chrono > 220.0f & chrono < 221.0f & textMedicament.enabled == true)
        {
            healthBar.Heal(40);
            textMedicament.enabled = false;
            textTemperature.enabled = true;
            thermometreHomme.GetComponent<Renderer>().material.mainTexture = rouge;
        }
        if(textTemperature.enabled == false)
        {
            thermometreHomme.GetComponent<Renderer>().material.mainTexture = marron;
        }


        if (chrono > 255.0f & chrono < 256.0f & textTemperature.enabled == true)
        {
            healthBar.Heal(20);
            textTemperature.enabled = false;
            textReanimation.enabled = true;
            reanimerHomme.GetComponent<Renderer>().material.mainTexture = rouge;
        }
        if(textReanimation.enabled == false)
        {
            reanimerHomme.GetComponent<Renderer>().material.mainTexture = marron;
        }


        if (chrono > 280.0f & chrono < 281.0f & textReanimation.enabled == true)
        {
            healthBar.Heal(0);
            //SceneManager.LoadScene("SceneGameOver");
        }




        //Troisième série de tâches
        if (chrono > 320.0f & chrono < 321.0f)
        {
            healthBar.Heal(50);
            textNourriture.enabled = true;
        }

        }

    // Start is called before the first frame update
    void Start()
    {
        

        textNourriture.enabled = false;
        textPerfusion.enabled = false;
        textTemperature.enabled = false;
        textMedicament.enabled = false;
        textReanimation.enabled = false;
        textPansement.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        chrono += Time.deltaTime;
        UpdateTasksToDo();
    }
}
