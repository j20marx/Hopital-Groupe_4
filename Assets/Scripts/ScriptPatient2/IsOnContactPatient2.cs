﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IsOnContactPatient2 : MonoBehaviour
{
    //Homme

    public float chronoPerfusion = 0.0f;
    public float chronoNourriture = 0.0f;
    public float chronoTemperature = 0.0f;
    public float chronoMedicament = 0.0f;
    public float chronoReanimation = 0.0f;
    public float chronoPansement = 0.0f;

    public Text textTemperature;
    public Text textPerfusion;
    public Text textNourriture;
    public Text textMedicament;
    public Text textReanimation;
    public Text textPansement;

    public HealthBar_2 healthBar;


    public void OnTriggerStay(Collider other)
    {
        if (other.gameObject.name == "Thermometre_Homme") 
        {
            chronoTemperature += Time.deltaTime;

            if (chronoTemperature >= 5.0f)
            {
                textTemperature.enabled = false;
                healthBar.Heal(100);
            }
        }
        if (other.gameObject.name == "Perfusion_Homme") 
        {
            chronoPerfusion += Time.deltaTime;
            if (chronoPerfusion >= 5.0f)
            {
                textPerfusion.enabled = false;
                healthBar.Heal(100);

            }
        }
        if (other.gameObject.name == "Food_Homme")
        {
            chronoNourriture += Time.deltaTime;

            if (chronoNourriture >= 5.0f)
            {
                textNourriture.enabled = false;
                healthBar.Heal(100);

            }
        }
        if (other.gameObject.name == "Medicament_Homme") 
        {
            chronoMedicament += Time.deltaTime;

            if (chronoMedicament >= 5.0f)
            {
                textMedicament.enabled = false;
                healthBar.Heal(100);

            }
        }
        if (other.gameObject.name == "Reanimer_Homme") 
        {
            chronoReanimation += Time.deltaTime;

            if (chronoReanimation >= 5.0f)
            {
                textReanimation.enabled = false;
                healthBar.Heal(100);

            }
        }
        if (other.gameObject.name == "Bandage_Homme")
        {
            chronoPansement += Time.deltaTime;

            if (chronoPansement >= 5.0f)
            {
                textPansement.enabled = false;
                healthBar.Heal(100);

            }
        }
    }

    public void OnTriggerExit(Collider other)
    {


        chronoTemperature = 0.0f;
        chronoPerfusion = 0.0f;
        chronoNourriture = 0.0f;
        chronoMedicament = 0.0f;
        chronoReanimation = 0.0f;
        chronoPansement = 0.0f;
    }


   
}
