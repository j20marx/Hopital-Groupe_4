﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScreenP4Controller : MonoBehaviour
{
    //Valable pour un seul patient : l'enfant

    public Text textPerfusion;
    public Text textTemperature;
    public Text textReanimation;
    public Text textTension;
    public Text textEau;

    public GameObject perfusionEnfant;
    public GameObject thermometreEnfant;
    public GameObject reanimerEnfant;
    public GameObject tensionEnfant;
    public GameObject eauEnfant;

    public float chrono = 0.0f;

    public GameObject enfant;

    public Healthbar_4 healthBar;

    public Texture blanc;
    public Texture rouge;
    public Texture marron;
    public Texture cyan;
    public Texture eau;

    public void UpdateTasksToDo()
    {
        //Première série de tâches
        if(chrono > 40.0f & chrono < 41.0f)
        {
            healthBar.Heal(75);
            tensionEnfant.GetComponent<Renderer>().material.mainTexture = rouge;
            textTension.enabled = true;   
        }
        if(textTension.enabled == false)
        {
            tensionEnfant.GetComponent<Renderer>().material.mainTexture = cyan;
        }


        if (chrono > 60.0f & chrono < 61.0f & textTension.enabled == true)
        {
            healthBar.Heal(50);
            textTension.enabled = false;
            textEau.enabled = true;
            eauEnfant.GetComponent<Renderer>().material.mainTexture = rouge;
        }
        if(textEau.enabled == false)
        {
            eauEnfant.GetComponent<Renderer>().material.mainTexture = eau;
        }


        if (chrono > 80.0f & chrono < 81.0f & textEau.enabled == true)
        {
            healthBar.Heal(25);
            textEau.enabled = false;
            textReanimation.enabled = true;
            reanimerEnfant.GetComponent<Renderer>().material.mainTexture = rouge;
        }
        if(textReanimation.enabled == false)
        {
            reanimerEnfant.GetComponent<Renderer>().material.mainTexture = marron;
        }


        if (chrono > 101.0f & chrono < 102.0f & textReanimation.enabled == true)
        {
            healthBar.Heal(0);
            reanimerEnfant.GetComponent<Renderer>().material.mainTexture = marron;
            SceneManager.LoadScene("SceneGameOver");
        }






        //Deuxième série de tâches
        if (chrono > 140.0f & chrono < 141.0f)
        {
            healthBar.Heal(80);
            eauEnfant.GetComponent<Renderer>().material.mainTexture = rouge;
            textEau.enabled = true;  
        }
        if(textEau.enabled == false)
        {
            eauEnfant.GetComponent<Renderer>().material.mainTexture = eau;
        }


        if (chrono > 160.0f & chrono < 161.0f & textEau.enabled == true)
        {
            healthBar.Heal(60);
            textEau.enabled = false;
            textTension.enabled = true;
            tensionEnfant.GetComponent<Renderer>().material.mainTexture = rouge;
        }
        if(textTension.enabled == false)
        {
            tensionEnfant.GetComponent<Renderer>().material.mainTexture = cyan;
        }


        if (chrono > 185.0f & chrono < 186.0f & textTension.enabled == true)
        {
            healthBar.Heal(40);
            textTension.enabled = false;
            textTemperature.enabled = true;
            thermometreEnfant.GetComponent<Renderer>().material.mainTexture = rouge;
        }
        if(textTemperature.enabled == false)
        {
            thermometreEnfant.GetComponent<Renderer>().material.mainTexture = blanc;
        }


        if (chrono > 205.0f & chrono < 206.0f & textTemperature.enabled == true)
        {
            healthBar.Heal(20);
            textTemperature.enabled = false;
            textPerfusion.enabled = true;
            perfusionEnfant.GetComponent<Renderer>().material.mainTexture = rouge;
        }
        if(textPerfusion.enabled == false)
        {
            perfusionEnfant.GetComponent<Renderer>().material.mainTexture = blanc;
        }
        if (chrono > 220.0f & chrono < 221.0f & textPerfusion.enabled == true)
        {
            healthBar.Heal(0);
            perfusionEnfant.GetComponent<Renderer>().material.mainTexture = blanc;
            SceneManager.LoadScene("SceneGameOver");
        }







        //Troisème série de tâches
        if (chrono > 285.0f & chrono < 286.0f)
        {
            healthBar.Heal(75);
            thermometreEnfant.GetComponent<Renderer>().material.mainTexture = rouge;
            textTemperature.enabled = true;          
        }
        if(textTemperature.enabled == false)
        {
            thermometreEnfant.GetComponent<Renderer>().material.mainTexture = blanc;
        }


        if (chrono > 305.0f & chrono < 306.0f & textTemperature.enabled == true)
        {
            healthBar.Heal(50);
            textTemperature.enabled = false;
            textPerfusion.enabled = true;
            perfusionEnfant.GetComponent<Renderer>().material.mainTexture = rouge;
        }
        if(textPerfusion.enabled == false)
        {
            perfusionEnfant.GetComponent<Renderer>().material.mainTexture = blanc;
        }


        if (chrono > 325.0f & chrono < 326.0f & textPerfusion.enabled == true)
        {
            healthBar.Heal(25);
            textPerfusion.enabled = false;
            textTension.enabled = true;
            tensionEnfant.GetComponent<Renderer>().material.mainTexture = rouge;
        }
        if(textTension.enabled == false)
        {
            tensionEnfant.GetComponent<Renderer>().material.mainTexture = cyan;
        }
        if (chrono > 350.0f & chrono < 351.0f & textTension.enabled == true)
        {
            healthBar.Heal(0);
            tensionEnfant.GetComponent<Renderer>().material.mainTexture = cyan;
            SceneManager.LoadScene("SceneGameOver");
        }

    }


    // Start is called before the first frame update
    void Start()
    {
        textPerfusion.enabled = false;
        textTemperature.enabled = false;
        textReanimation.enabled = false;
        textTension.enabled = false;
        textEau.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        chrono += Time.deltaTime;
        UpdateTasksToDo();
    }
}

