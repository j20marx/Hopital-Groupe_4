﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IsOnContactPatient4 : MonoBehaviour
{
    public float chronoPerfusion = 0.0f;
    public float chronoEau = 0.0f;
    public float chronoTemperature = 0.0f;
    public float chronoReanimation = 0.0f;
    public float chronoTension = 0.0f;

    public Text textTemperature;
    public Text textPerfusion;
    public Text textEau;
    public Text textReanimation;
    public Text textTension;

    public Healthbar_4 healthBar;

    public void OnTriggerStay(Collider other)
    {
        if (other.gameObject.name == "Thermometre_Enfant")
        {
            chronoTemperature += Time.deltaTime;
            gameObject.GetComponent<MeshRenderer>().material.color = Color.yellow;
            if (chronoTemperature >= 5.0f)
            {
                textTemperature.enabled = false;
                healthBar.Heal(100);
                gameObject.GetComponent<MeshRenderer>().material.color = Color.green;
            }
        }
        if (other.gameObject.name == "Perfusion_Enfant") 
        {
            chronoPerfusion += Time.deltaTime;
            gameObject.GetComponent<MeshRenderer>().material.color = Color.yellow;
            if (chronoPerfusion >= 5.0f)
            {
                textPerfusion.enabled = false;
                healthBar.Heal(100);
                gameObject.GetComponent<MeshRenderer>().material.color = Color.green;
            }
        }
        if (other.gameObject.name == "Eau_Enfant")
        {
            chronoEau += Time.deltaTime;
            gameObject.GetComponent<MeshRenderer>().material.color = Color.yellow;
            if (chronoEau >= 5.0f)
            {
                textEau.enabled = false;
                healthBar.Heal(100);
                gameObject.GetComponent<MeshRenderer>().material.color = Color.green;
            }
        }
        if (other.gameObject.name == "Reanimer_Enfant") 
        {
            chronoReanimation += Time.deltaTime;
            gameObject.GetComponent<MeshRenderer>().material.color = Color.yellow;
            if (chronoReanimation >= 5.0f)
            {
                textReanimation.enabled = false;
                healthBar.Heal(100);
                gameObject.GetComponent<MeshRenderer>().material.color = Color.green;
            }
        }

        if (other.gameObject.name == "Tension_Enfant") 
        {
            chronoTension += Time.deltaTime;
            gameObject.GetComponent<MeshRenderer>().material.color = Color.yellow;
            if (chronoTension >= 5.0f)
            {
                textTension.enabled = false;
                healthBar.Heal(100);
                gameObject.GetComponent<MeshRenderer>().material.color = Color.green;
            }
        }
    }

    public void OnTriggerExit(Collider other)
    {
        gameObject.GetComponent<MeshRenderer>().material.color = new Color(0, 0, 0, 0);

        chronoTemperature = 0.0f;
        chronoPerfusion = 0.0f;
        chronoEau = 0.0f;
        chronoReanimation = 0.0f;
        chronoReanimation = 0.0f;
        chronoTension = 0.0f;
    }
  
}
