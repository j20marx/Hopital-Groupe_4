﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StartToMain : MonoBehaviour
{
    public string introduction;
    public int flag = 0;
    public string ChangeIntroduction(int a)
    {
        if (a == 0)
        {
            introduction = "Introduction\n\nBonjour et bienvenue dans la simulation ! \n\nAujourd'hui, dans ce casque, vous êtes un.e infirmier.e en charge de 4 patients. Ces patients ont besoin de vous pour survivre, parviendrez-vous à les maintenir en vie ? Combien de temps ? ";
        }

        else if (a == 1)
        { 
            introduction = "Introduction\n\n4 patient avec 4 lits vous attendent, un écran au dessus de chacun d'eux vous indique s'ils ont besoin d'un soin, et s'ils sont en forme. À côté d'eux se trouvent du matériel. Ne vous inquiétez pas, vous n'aurez pas besoin de faire des gestes techniques, l'utilisation des outils est facile. \n\nN'oubliez-pas de remettre les objets à leur place, ne les perdez pas. ";
        }
        else if (a == 2)
        {
            introduction = "Introduction\n\nLe chronomètre va se lancer, bonne chance à vous, prenez soin de vos patients, soyez gentils avec eux, ils sont malades. Leurs vies sont en dangers, si vous ne faites pas les bons soins à temps, le patient meurt, vous aurez fauté.\n\nAttention, pour valider une tache vous devez toucher le patient pendant 5 secondes avec le materiel necessaire. ";
        }
        else if (a == 3)
        {
            introduction = "Introduction\n\nSi vous êtes prêt, cliquez sur le bouton 'Skip' pour démarrer le jeu.";
        }
        else
        {
            introduction = "Introduction\n\nBonjour et bienvenue dans la simulation! \n\nAujourd'hui, dans ce casque, vous êtes un.e infirmier.e en charge de 4 patients. Ces patients ont besoin de vous pour survivre, parviendrez-vous à les maintenir en vie ? Combien de temps ? ";
        }
        return introduction;

    }
    // Use this for initialization
    void Start()
    {
        introduction = "Introduction\n\nBonjour et bienvenue dans la simulation! \n\nAujourd'hui, dans ce casque, vous êtes un.e infirmier.e en charge de 4 patients. Ces patients ont besoin de vous pour survivre, parviendrez-vous à les maintenir en vie ? Combien de temps ? ";
        GameObject.Find("Canvas/Panel/Image/Text").GetComponent<Text>().text = introduction;
    }

    // Update is called once per frame
    void Update()
    {
        GameObject.Find("Canvas/Panel/Image/Text").GetComponent<Text>().text = introduction;
    }

    public void OnStartGame(string sceneName)
    {
        flag = 0;
        Application.LoadLevel(sceneName);
        //start timer in sceneName
    }

    public void AddText()
    {
        flag = flag + 1;
        if (flag == 4) {
            flag = 0;
        }
        ChangeIntroduction(flag % 4);
        GameObject.Find("Canvas/Panel/Image/Text").GetComponent<Text>().text = introduction;
    }
    public void MinusText()
    {
        flag = flag - 1;
        if (flag == 0)
        {
            flag = 4;
        }
        else if (flag == -1)
        {
            flag = 3;
        }
        ChangeIntroduction(flag % 4);
        GameObject.Find("Canvas/Panel/Image/Text").GetComponent<Text>().text = introduction;
    }

}
