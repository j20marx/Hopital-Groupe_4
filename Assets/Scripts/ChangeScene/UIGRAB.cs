﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;


//using Dilmer's Script:  https://www.youtube.com/watch?v=IWuCrVMcYLc&ab_channel=DilmerValecillos

/* STILL NEED TO ADD:
 * SCENE CHANGES
 * IMAGES ON CHECKLIST
 * COUNTERS ON CHECKLIST
 * INDOOR/OUTDOOR(?) MAP(S)
 * AESTHETICS/RECOLORINGS
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

public class UIGRAB : MonoBehaviour
{

    //public List<XRController> controllers = null;

    /*[SerializeField]
    private RawImage background;
    */
    [SerializeField]
    private string sceneName;
    [SerializeField]
    private Button NextButton;


    [SerializeField]
    private Button LastButton;

    [SerializeField]
    private Button SkipButton;

    public string introduction;
    public int flag = 0;
    public string ChangeIntroduction(int a)
    {
        if (a == 0)
        {
            introduction = "introduction1";
        }

        else if (a == 1)
        {
            introduction = "introduction2";
        }
        else if (a == 2)
        {
            introduction = "introduction3";
        }
        else if (a == 3)
        {
            introduction = "introduction4";
        }
        else
        {
            introduction = "introduction1";
        }
        return introduction;

    }

    void Start()
    {
        introduction = "introduction1";
        GameObject.Find("Canvas/Panel/Image/Text").GetComponent<Text>().text = introduction;
        NextButton.onClick.AddListener(() =>
        {

            flag = flag + 1;
            if (flag == 4)
            {
                flag = 0;
            }
            ChangeIntroduction(flag % 4);
            GameObject.Find("Canvas/Panel/Image/Text").GetComponent<Text>().text = introduction;
        });
        LastButton.onClick.AddListener(() =>
        {
            flag = flag - 1;
            if (flag == 0)
            {
                flag = 4;
            }
            ChangeIntroduction(flag % 4);
            GameObject.Find("Canvas/Panel/Image/Text").GetComponent<Text>().text = introduction;
        });
        SkipButton.onClick.AddListener(() =>
        {
            flag = 0;
            Application.LoadLevel(sceneName);
            //start timer in sceneName
        });
        //checklist.gameObject.SetActive(false);
        //map.gameObject.SetActive(false);



        //checklistButton.onClick.AddListener(() => //when checklist button is clicked
        //{
        //mainPanel.gameObject.SetActive(!mainPanel.gameObject.activeSelf); //deactivates core panel
        //checklist.gameObject.SetActive(!checklist.gameObject.activeSelf); //change the state of the checklist (to the opposite state)

        /*checklistButton.GetComponentInChildren<TextMeshProUGUI>().text = checklist.gameObject.activeSelf ? //set the child text of the button equal to 
           "CHECKLIST ON" : "CHECKLIST OFF"; //whichever it is not*/


        // }); 


        //NextButton.onClick.AddListener(() =>
        //{
        //mainPanel.gameObject.SetActive(!mainPanel.gameObject.activeSelf);
        //map.gameObject.SetActive(!map.gameObject.activeSelf);

        //mapButton.GetComponentInChildren<TextMeshProUGUI>().text = map.gameObject.activeSelf ?
        //"MAP ON" : "MAP OFF";
        //yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy67666});








        // __________________________________________________________________________________________________________________________







        /*lightButton.onClick.AddListener(() =>
        {
            if (skyMaterial.GetFloat("_Exposure") > 0)
            {
                skyMaterial.SetFloat("_Exposure", 0);
                lightButton.GetComponentInChildren<TextMeshProUGUI>().text = "NIGHT TIME";
            }
            else
            {
                skyMaterial.SetFloat("_Exposure", 1.3f);
                lightButton.GetComponentInChildren<TextMeshProUGUI>().text = "DAY TIME";
            }
        });
        alphaToggle.onValueChanged.AddListener((value) =>
        {
            Color currColor = background.color;
            currColor.a = value ? 0.7f : 0;
            background.color = currColor;
        });
        fontSlider.onValueChanged.AddListener((value) =>
        {
            creditsInfoDetails.fontSize = value;
            fontText.text = $"FONT SIZE: {value}";
        });*/
    }
}